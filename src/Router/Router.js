import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { HOME, REGISTRATION } from '../constants/RouteConstants';
import Home from '../Container/Home';
import Registration from '../Container/Registration/Registration';

const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/">
        <Redirect to={HOME} />
      </Route>
      <Route path={HOME} component={Home} />
      <Route path={REGISTRATION} component={Registration} />
    </Switch>
  </BrowserRouter>
);

export default Router;