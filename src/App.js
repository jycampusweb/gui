import React from 'react';
import { IntlProvider } from 'react-intl';
import BrowserRouter from './Router';

const App = () => (
  <IntlProvider locale="en">
    <BrowserRouter />
  </IntlProvider>
);
export default App;
