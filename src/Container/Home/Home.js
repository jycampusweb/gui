/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import MenuIcon from '@material-ui/icons/Menu';
import SideDrawer from '../../Components/SideDrawer';
import IconButton from '../../Components/Buttons/IconButton';
import styles from './Home.module.css'

const HamBurgerIcon = ({ onClick }) => (
  <div className={styles.btnClass} >
    <IconButton onClick={onClick}>
      <MenuIcon style={{ color:"#305F7A" }} size="medium"/>
    </IconButton>
  </div>
);

const Home = () => {
  const [isDrawerOpen, setDrawerOpen] = useState(false);
  const drawerCloseHandler = () => setDrawerOpen(false);
  const drawerOpenHandler = () => setDrawerOpen(true);
  return (
  <>
    {!isDrawerOpen && <HamBurgerIcon onClick={drawerOpenHandler}/>}
    <SideDrawer onClose={drawerCloseHandler} isOpen={isDrawerOpen}/>
  </>
  );
};

export default Home;