import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Drawer from '@material-ui/core/Drawer';
import CustomButton from './CustomButton';
import RouteMap from '../../Router/RouteMap';
import Header from './Header';

import styles from './SideDrawer.module.css';

const SideDrawer = ({ onClose, isOpen, history }) => {
  const itemClickHandler = (path) => history.push(path);
  const classes = {
    paperAnchorDockedLeft: styles.noBorder
  }
  return (
    <Drawer
      anchor="left"
      variant="persistent"
      open={isOpen}
      classes={classes}
    >
      <div className={styles.drawerContainer}>
        <Header onClose={onClose} />
        {RouteMap.map(({ text, path }) =>
          <CustomButton text={text} key={path} onClick={() => itemClickHandler(path)} /> )}
      </div>
    </Drawer>
  );
};

SideDrawer.propTypes = {
  onClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  history: PropTypes.object.isRequired
}

export default withRouter(SideDrawer);