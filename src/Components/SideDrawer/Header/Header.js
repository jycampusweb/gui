import React from 'react';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '../../Buttons/IconButton';

const Header = ({ onClose }) => (
  <>
    <IconButton onClick={onClose}>
      <CloseIcon style={{ color:"#305F7A" }} />
    </IconButton>
    <span>
      <FormattedMessage defaultMessage="MENU" description="heading indicating menu options" id="menu.menu" />
    </span>
  </>
);

Header.propTypes = {
  onClose: PropTypes.func.isRequired
}

export default Header;