/* eslint-disable react/prop-types */
import React from 'react';

import styles from './CustomButton.module.css';

const CustomButton = ({ text, onClick }) => (
  <div className={styles.btn} onClick={onClick}>
    {text}
  </div>
)

export default CustomButton;